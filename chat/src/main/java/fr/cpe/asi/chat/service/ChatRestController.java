package fr.cpe.asi.chat.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;
import fr.cpe.asi.data.model.rest.ChatRest;


@CrossOrigin
@RestController
public class ChatRestController implements ChatRest {

    private Logger logger = LoggerFactory.getLogger(ChatRestController.class);
	private ChatService chatService;
	
	public ChatRestController(ChatService chatService) {
		this.chatService = chatService;
	}

	@Override
	public ResponseEntity<HttpStatus> addMessage(ChatModel chat) {
		try {
			chatService.addMessage(chat); 
			return new ResponseEntity<>(HttpStatus.OK, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<List<ChatModel>> getConversation(int emitterId, int receiverId) {
		return new ResponseEntity<>(chatService.getConversation(emitterId, receiverId), HttpStatus.OK);
	}
	



}
