package fr.cpe.asi.chat.jms;

import javax.jms.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;
import fr.cpe.asi.data.model.jms.TypeOperation;

@Service
public class JmsService {

	public static final String JMS_QUEUE_CHAT = "chat";
	private JmsTemplate jmsTemplate;
	private Queue queueChat;

    private Logger logger = LoggerFactory.getLogger(JmsService.class);

	public JmsService(JmsTemplate jmsTemplate, Queue queueCard) {
		this.jmsTemplate = jmsTemplate;
		this.queueChat = queueCard;
	}
	
	public void createChat(ChatModel chat) {
		logger.info("Envoi du message {} sur {}", chat, JMS_QUEUE_CHAT);
		jmsTemplate.convertAndSend(queueChat, new ChatOperation(TypeOperation.POST, chat));
	}
	
}
