package fr.cpe.asi.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.jms.annotation.EnableJms;

/**
 * @author 
 *
 */
@EnableJms
@SpringBootApplication
@EntityScan("fr.cpe.asi.data.model.entities.chat")
public class ChatApplication {
	
	public static void main(String[] args) {
        SpringApplication.run(ChatApplication.class, args);
    }

}
