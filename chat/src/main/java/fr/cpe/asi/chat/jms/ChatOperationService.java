package fr.cpe.asi.chat.jms;

import org.springframework.stereotype.Service;

import fr.cpe.asi.chat.service.ChatRepository;
import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;

@Service
public class ChatOperationService {

	private ChatRepository chatRepository;
	
	public ChatOperationService(ChatRepository chatRepository) {
		this.chatRepository = chatRepository;
	}

	public void createChat(ChatModel chat) {
		chatRepository.save(chat);
	}

	
}
