package fr.cpe.asi.chat.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;


public interface ChatRepository extends JpaRepository<ChatModel, Integer> {

	
    List<ChatModel> findByEmitterIdAndReceiverId(int emitterId, int receiverId);

}
