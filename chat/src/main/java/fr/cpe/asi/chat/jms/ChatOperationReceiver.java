package fr.cpe.asi.chat.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;


@Component
public class ChatOperationReceiver {
	
    private Logger logger = LoggerFactory.getLogger(ChatOperationReceiver.class);
	private ChatOperationService chatOperationService;
	
	public ChatOperationReceiver(ChatOperationService chatOperationService) {
		this.chatOperationService = chatOperationService;
	}

	@JmsListener(destination = "chat")
	public void receiveCardOperation(ChatOperation chatOperation) {
		logger.info("Réception du message {} sur {}", chatOperation, JmsService.JMS_QUEUE_CHAT);

		switch (chatOperation.getTypeOperation()) {
			case POST:
				chatOperationService.createChat(chatOperation.getChat());
				break;
			default:
				break;
		}
	}

}

