package fr.cpe.asi.chat.jms;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;
import fr.cpe.asi.data.model.jms.Operation;
import fr.cpe.asi.data.model.jms.TypeOperation;

public class ChatOperation extends Operation{

	private ChatModel chat;
	
	public ChatOperation(TypeOperation typeOperation, ChatModel chat) {
		this.chat = chat;
		this.typeOperation = typeOperation;
	}


	public ChatModel getChat() {
		return chat;
	}

	public void setChat(ChatModel chat) {
		this.chat = chat;
	}
	
	
}
