package fr.cpe.asi.chat.service;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.cpe.asi.chat.jms.JmsService;
import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;


@Service
public class ChatService {
	
	private ChatRepository chatRepository;
	private JmsService jmsService;
	
	public ChatService(ChatRepository chatRepository, JmsService jmsService) {
		this.chatRepository = chatRepository;
		this.jmsService = jmsService;
	}

	public List<ChatModel> getConversation(int emitterId, int receiverId) {
		List<ChatModel> listMessages = chatRepository.findByEmitterIdAndReceiverId(emitterId, receiverId);
		return listMessages;
	}
	


	public void addMessage(ChatModel chat) {
		jmsService.createChat(chat);

	}

	
}

