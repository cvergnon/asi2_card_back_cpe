package fr.cpe.asi.card.jms;

import javax.jms.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.jms.TypeOperation;

@Service
public class JmsService {

	public static final String JMS_QUEUE_CARD = "card";
	private JmsTemplate jmsTemplate;

    private Logger logger = LoggerFactory.getLogger(JmsService.class);

	public JmsService(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}
	
	public void updateCard(CardModel card) {
		logger.info("Envoi du message {} sur {}", card, JMS_QUEUE_CARD);
		jmsTemplate.convertAndSend(JMS_QUEUE_CARD, new CardOperation(TypeOperation.PUT, card));
	}
	
	public void deleteCard(CardModel card) {
		logger.info("Envoi du message {} sur {}", card, JMS_QUEUE_CARD);
		jmsTemplate.convertAndSend(JMS_QUEUE_CARD, new CardOperation(TypeOperation.DELETE, card));
	}
	
	public void createCard(CardModel card) {
		logger.info("Envoi du message {} sur {}", card, JMS_QUEUE_CARD);
		jmsTemplate.convertAndSend(JMS_QUEUE_CARD, new CardOperation(TypeOperation.POST, card));
	}
	
}
