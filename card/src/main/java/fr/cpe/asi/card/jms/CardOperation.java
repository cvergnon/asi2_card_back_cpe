package fr.cpe.asi.card.jms;

import java.io.Serializable;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.jms.Operation;
import fr.cpe.asi.data.model.jms.TypeOperation;

public class CardOperation extends Operation{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6958892134588598934L;
	
	private CardModel card;
	
	public CardOperation() {
		
	}
	
	public CardOperation(TypeOperation typeOperation, CardModel card) {
		this.card = card;
		this.typeOperation = typeOperation;
	}

	

	public CardModel getCard() {
		return card;
	}

	public void setCard(CardModel card) {
		this.card = card;
	}
	
	
}
