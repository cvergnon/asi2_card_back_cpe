package fr.cpe.asi.card.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.cpe.asi.data.model.entities.card.CardModel;


public interface CardRepository extends JpaRepository<CardModel, Integer> {


    List<CardModel> findByUserId(Integer userId);
}
