package fr.cpe.asi.card.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import fr.cpe.asi.card.jms.JmsService;
import fr.cpe.asi.data.model.entities.card.CardModel;


@Service
public class CardService {
	
	private CardRepository cardRepository;
	private JmsService jmsService;
	
	public CardService(CardRepository cardRepository, JmsService jmsService) {
		this.cardRepository = cardRepository;
		this.jmsService = jmsService;
	}

	public List<CardModel> getAllCardModel() {
		List<CardModel> cardList = new ArrayList<>();
		cardRepository.findAll().forEach(cardList::add);
		return cardList;
	}

	public void addCard(CardModel cardModel) {
		jmsService.createCard(cardModel);
	}

	
	public void updateCard(CardModel cardModel) {
		cardRepository.save(cardModel);
	}
	
	public Optional<CardModel> getCard(Integer id) {
		return cardRepository.findById(id);
	}
	
	public void deleteCard(Integer id) {
		CardModel card = new CardModel();
		card.setId(id);
		jmsService.deleteCard(card);
	}
	
	public List<CardModel> getAllCardsToSell() {
		return cardRepository.findAll()
				.stream()
				.filter(c -> c.getUserId() <= 0)
				.collect(Collectors.toList());
	}

	
	
	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
		CardModel cardModel = new CardModel("Zemmour", "Fourbe petit gobelin", "Extreme droite", "Jean Messiha, Charles de Gaule, Napoléon", "Débat burkini", (float)180.0,
					 (float)12.0, (float)3.0,"https://upload.wikimedia.org/wikipedia/commons/f/f0/%C3%89ric_Zemmour_10-2021.jpg",200);
		this.addCard(cardModel); 
		
		cardModel = new CardModel("Poutou", "Noble travailleur", "Extreme gauche", "Staline, Lenine, Marx", "Réduction du temps de travail", (float)180.0,
				 (float)12.0, (float)3.0,"https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Philippe_Poutou_2011_%28cropped_2%29.jpg/170px-Philippe_Poutou_2011_%28cropped_2%29.jpg",200);
		this.addCard(cardModel); 
		
		cardModel = new CardModel("Macron", "Ex-banquier", "En Marche", "Marlène Schiappa, Darmanin", "Augmentation des retraites", (float)180.0,
				 (float)12.0, (float)3.0,"https://upload.wikimedia.org/wikipedia/commons/c/c3/Emmanuel_Macron_%28cropped%29.jpg",200);
		this.addCard(cardModel); 
		
		cardModel = new CardModel("De Gaulle", "L'ancètre", "Droite", "Pompidou, Michel Debré", "Libération", (float)180.0,
				 (float)12.0, (float)3.0,"https://upload.wikimedia.org/wikipedia/commons/4/41/De_Gaulle_1961_%28cropped%29.jpg",200);
		this.addCard(cardModel); 
	}

	public List<CardModel> getCardsByUser(Integer userId) {
		return cardRepository.findByUserId(userId);
	}

}

