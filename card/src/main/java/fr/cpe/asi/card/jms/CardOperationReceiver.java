package fr.cpe.asi.card.jms;

import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;


@Component
public class CardOperationReceiver {
	
    private Logger logger = LoggerFactory.getLogger(CardOperationReceiver.class);
	private CardOperationService cardOperationService;
	
	public CardOperationReceiver(CardOperationService cardOperationService) {
		this.cardOperationService = cardOperationService;
	}
	
	@JmsListener(destination = JmsService.JMS_QUEUE_CARD, containerFactory = "connectionFactory")
	public void receiveCardOperation(CardOperation cardOperation, Message message) {
		logger.info("Réception du message {} sur {}", cardOperation, JmsService.JMS_QUEUE_CARD);

		switch (cardOperation.getTypeOperation()) {
			case DELETE:
				cardOperationService.deleteCard(cardOperation.getCard().getId());
				break;
			case POST:
				cardOperationService.createCard(cardOperation.getCard());
				break;
			case PUT:
				cardOperationService.updateCard(cardOperation.getCard());
				break;
			default:
				break;
		}
	}

}

