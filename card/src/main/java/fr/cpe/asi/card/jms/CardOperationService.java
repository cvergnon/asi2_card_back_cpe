package fr.cpe.asi.card.jms;

import org.springframework.stereotype.Service;

import fr.cpe.asi.card.service.CardRepository;
import fr.cpe.asi.data.model.entities.card.CardModel;

@Service
public class CardOperationService {

	private CardRepository cardRepository;
	
	public CardOperationService(CardRepository cardRepository) {
		this.cardRepository = cardRepository;
	}

	public void deleteCard(Integer id) {
		cardRepository.deleteById(id);
	}

	public void createCard(CardModel card) {
		cardRepository.save(card);
	}

	public void updateCard(CardModel card) {
		cardRepository.save(card);
	}
	
}
