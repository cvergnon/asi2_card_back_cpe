package fr.cpe.asi.card;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;


@EnableJms
@SpringBootApplication
@EntityScan("fr.cpe.asi.data.model.entities.card")
@ComponentScan(basePackages = {"fr.cpe.asi.card", "fr.cpe.asi.card.jms"})
@EnableAutoConfiguration
public class CardApplication {

	public static void main(String[] args) {
        SpringApplication.run(CardApplication.class, args);
    }

}
