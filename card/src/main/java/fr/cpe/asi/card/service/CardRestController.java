package fr.cpe.asi.card.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.rest.CardRest;


@CrossOrigin
@RestController
@RequestMapping(value = CardRest.BASE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class CardRestController implements CardRest {

    private Logger logger = LoggerFactory.getLogger(CardRestController.class);
	private CardService cardService;
	
	public CardRestController(CardService cardService) {
		this.cardService = cardService;
	}
	
	@Override
	@RequestMapping(
    		value = GET_CARDS,
    		method = RequestMethod.GET
    )
	public ResponseEntity<List<CardModel>> getAllCards() {
		logger.trace("Call getAllCards()");
		return new ResponseEntity<>(cardService.getAllCardModel(), HttpStatus.OK);
	}

	@Override
    @RequestMapping(
    		value = GET_CARD_BY_ID,
    		method = RequestMethod.GET
    )
	public ResponseEntity<CardModel> getCard(@PathVariable Integer id) {
		logger.trace("Call getCard({})", id);
		Optional<CardModel> optCardModel = cardService.getCard(id);
		if(optCardModel.isPresent()) {
			return new ResponseEntity<>(optCardModel.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Override
    @RequestMapping(
    		value = ADD_CARD,
    		method = RequestMethod.POST
    )
	public ResponseEntity<HttpStatus> addCard(@RequestBody CardModel card) {
		logger.trace("Call addCard({})", card);
		cardService.addCard(card);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
    @RequestMapping(
    		value = UPDATE_CARD,
    		method = RequestMethod.PUT
    )
	public ResponseEntity<HttpStatus> updateCard(@RequestBody CardModel card, @PathVariable Integer id) {
		logger.trace("Call updateCard({})", card);
		card.setId(id);
		cardService.updateCard(card);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	@RequestMapping(
    		value = DELETE_CARD,
    		method = RequestMethod.DELETE
    )
	public ResponseEntity<HttpStatus> deleteCard(@PathVariable Integer id) {
		logger.trace("Call deleteCard({})", id);
		cardService.deleteCard(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
    @RequestMapping(
    		value = GET_CARDS_TO_SELL,
    		method = RequestMethod.GET
    )
	public ResponseEntity<List<CardModel>> getCardsToSell() {
		logger.trace("Call getCardsToSell()");
		return new ResponseEntity<>(cardService.getAllCardsToSell(), HttpStatus.OK);
	}

	
	@Override
    @RequestMapping(
    		value = GET_CARDS_BY_USER,
    		method = RequestMethod.GET
    )
	public ResponseEntity<List<CardModel>> getCardsByUser(@PathVariable Integer userId) {
		return new ResponseEntity<>(cardService.getCardsByUser(userId), HttpStatus.OK);
	}
	
}
