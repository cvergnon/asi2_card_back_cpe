# ASI2_Card_Back_Cpe

docker run --name postgresql-container -p 5432:5432 -e POSTGRES_PASSWORD=ASI_PASSWORD -d postgres  

Récupérer l'identifiant du container avec **docker ps**  

docker exec -it **ID_CONTAINER** bash  
psql -U postgres  
create database card  
create database userdb  
create database chat  

docker run -p 61616:61616 -p 8161:8161 rmohr/activemq
