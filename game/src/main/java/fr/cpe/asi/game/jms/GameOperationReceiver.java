package fr.cpe.asi.game.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;


@Component
public class GameOperationReceiver {
	
    private Logger logger = LoggerFactory.getLogger(GameOperationReceiver.class);
	private GameOperationService gameOperationService;
	
	public GameOperationReceiver(GameOperationService gameOperationService) {
		this.gameOperationService = gameOperationService;
	}

	@JmsListener(destination = "game")
	public void receiveCardOperation(GameOperation gameOperation) {
		logger.info("Réception du message {} sur {}", gameOperation, JmsService.JMS_QUEUE_GAME);

		switch (gameOperation.getTypeOperation()) {
			case POST:
				gameOperationService.createGame(gameOperation.getGame());
				break;
			default:
				break;
		}
	}

}

