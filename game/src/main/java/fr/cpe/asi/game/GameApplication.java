package fr.cpe.asi.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;

/**
 * @author 
 *
 */
@EnableJms
@SpringBootApplication
@ComponentScan(basePackages = {"fr.cpe.asi.game", "fr.cpe.asi.data.model.api.impl"})
@EntityScan("fr.cpe.asi.data.model.entities.game")
public class GameApplication {
	
	@Autowired
    JmsTemplate jmsTemplate;
	
	@EventListener(ApplicationReadyEvent.class)
    public void doInitAfterStartup() {
        jmsTemplate.setPubSubDomain(true);
    }


	public static void main(String[] args) {
        SpringApplication.run(GameApplication.class, args);
    }

}
