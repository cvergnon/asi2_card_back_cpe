package fr.cpe.asi.game.jms;

import javax.jms.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;
import fr.cpe.asi.data.model.entities.game.GameModel;
import fr.cpe.asi.data.model.jms.TypeOperation;

@Service
public class JmsService {

	public static final String JMS_QUEUE_GAME = "game";
	private JmsTemplate jmsTemplate;
	private Queue queueGame;

    private Logger logger = LoggerFactory.getLogger(JmsService.class);

	public JmsService(JmsTemplate jmsTemplate, Queue queueGame) {
		this.jmsTemplate = jmsTemplate;
		this.queueGame = queueGame;
	}
	
	public void createGame(GameModel game) {
		logger.info("Envoi du message {} sur {}", game, JMS_QUEUE_GAME);
		jmsTemplate.convertAndSend(queueGame, new GameOperation(TypeOperation.POST, game));
	}
	
}
