package fr.cpe.asi.game.jms;

import org.springframework.stereotype.Service;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;
import fr.cpe.asi.data.model.entities.game.GameModel;
import fr.cpe.asi.game.service.GameRepository;

@Service
public class GameOperationService {

	private GameRepository gameRepository;
	
	public GameOperationService(GameRepository gameRepository) {
		this.gameRepository = gameRepository;
	}

	public void createGame(GameModel game) {
		gameRepository.save(game);
		
	}

	
}
