package fr.cpe.asi.game.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;
import fr.cpe.asi.data.model.entities.game.GameModel;
import fr.cpe.asi.data.model.rest.ChatRest;
import fr.cpe.asi.data.model.rest.GameRest;


@CrossOrigin
@RestController
public class GameRestController implements GameRest {

    private Logger logger = LoggerFactory.getLogger(GameRestController.class);
	private GameService gameService;
	
	public GameRestController(GameService gameService) {
		this.gameService = gameService;
	}

	/**
	 * Ajout d'un message dans la base
	 */
    @RequestMapping(
    		value = ADD_GAME,
    		method = RequestMethod.POST
    )
	@Override
	public ResponseEntity<HttpStatus> addGame(GameModel game) {
		try {
			gameService.addGame(game); 
			return new ResponseEntity<>(HttpStatus.OK, HttpStatus.OK); 
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}

	
	



}
