package fr.cpe.asi.game.jms;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;
import fr.cpe.asi.data.model.entities.game.GameModel;
import fr.cpe.asi.data.model.jms.Operation;
import fr.cpe.asi.data.model.jms.TypeOperation;

public class GameOperation extends Operation {

	private GameModel game;
	
	public GameOperation(TypeOperation typeOperation, GameModel game) {
		this.game = game;
		this.typeOperation = typeOperation;
	}

	
	public GameModel getGame() {
		return game;
	}

	public void setGame(GameModel game) {
		this.game = game;
	}
	
	
}
