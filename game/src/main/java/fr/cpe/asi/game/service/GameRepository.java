package fr.cpe.asi.game.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;
import fr.cpe.asi.data.model.entities.game.GameModel;


public interface GameRepository extends JpaRepository<GameModel, Integer> {
}
