package fr.cpe.asi.game.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.cpe.asi.data.model.api.impl.CardImpl;
import fr.cpe.asi.data.model.api.impl.UserImpl;
import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;
import fr.cpe.asi.data.model.entities.game.GameModel;
import fr.cpe.asi.data.model.entities.user.UserModel;
import fr.cpe.asi.game.jms.JmsService;


@Service
public class GameService {
	
	private UserImpl userDAO;
	
	private GameRepository gameRepository;
	private JmsService jmsService;
	
	public GameService(GameRepository gameRepository, JmsService jmsService, UserImpl userDAO) {
		this.gameRepository = gameRepository;
		this.jmsService = jmsService;
		this.userDAO = userDAO;
	}

	
	public void addGame(GameModel game) {
		jmsService.createGame(game);
		this.updatePlayersBalance(game);
	}


	private void updatePlayersBalance(GameModel game) {
		UserModel winner = userDAO.getUser(game.getWinnerId()).getBody();
		winner.setAccount(winner.getAccount() + game.getBet());
		UserModel loser = userDAO.getUser(game.getLoserId()).getBody(); 
		loser.setAccount(loser.getAccount() + game.getBet());
		userDAO.updateUser(winner, winner.getId());
		userDAO.updateUser(loser, loser.getId());
	}
	
}

