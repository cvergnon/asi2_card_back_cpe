package fr.cpe.asi.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;


@EnableJms
@SpringBootApplication
@ComponentScan(basePackages = {"fr.cpe.asi.store", "fr.cpe.asi.data.model.api.impl"})
@EnableAutoConfiguration
public class StoreApplication {
	

	public static void main(String[] args) {
        SpringApplication.run(StoreApplication.class, args);
    }
}
