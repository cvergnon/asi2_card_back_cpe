package fr.cpe.asi.store.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.cpe.asi.data.model.dto.StoreOrderDTO;
import fr.cpe.asi.data.model.rest.StoreRest;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
@RequestMapping(value = StoreRest.BASE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class StoreRestController implements StoreRest{

	private StoreService storeService;
	
	public StoreRestController(StoreService storeService) {
		this.storeService = storeService;
	}
	
	
	@Override
    @RequestMapping(
    		value = BUY_CARDS,
    		method = RequestMethod.POST
    )
	public ResponseEntity<Boolean> buyCards(@RequestBody StoreOrderDTO order) {
		try {
			return new ResponseEntity<>(storeService.buyCard(order.getUserId(), order.getCardId()), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
    @RequestMapping(
    		value = SELL_CARDS,
    		method = RequestMethod.POST
    )
	public ResponseEntity<Boolean> sellCards(@RequestBody StoreOrderDTO order) {
		try {
			return new ResponseEntity<>(storeService.sellCard(order.getUserId(), order.getCardId()), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
