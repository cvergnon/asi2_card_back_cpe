package fr.cpe.asi.store.service;

import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import fr.cpe.asi.data.model.api.impl.CardImpl;
import fr.cpe.asi.data.model.api.impl.UserImpl;
import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.user.UserModel;

@Service
public class StoreService {
	
	private CardImpl cardDAO;
	private UserImpl userDAO;
	
	public StoreService(CardImpl cardDAO, UserImpl userDAO) {
		this.cardDAO = cardDAO;
		this.userDAO = userDAO;
	}
	
	
	public boolean buyCard(Integer userId, Integer cardId) throws Exception {
		CardModel cardModel = null;
		UserModel userModel = null;
		
		try {
			userModel = getUserModel(userId);
			cardModel = getCardModel(cardId);
		} catch (Exception e) {
			throw new Exception(e);
		}
		
		if (userModel.getAccount() > cardModel.getPrice()) {
			cardModel.setUserId(userId);
			cardDAO.updateCard(cardModel, cardId); 
			userModel.setAccount(userModel.getAccount() - cardModel.getPrice());
			List<Integer> listCardId = userModel.getCardList();
			listCardId.add(cardId);
			userModel.setCardList(listCardId);
			userDAO.updateUser(userModel, userId);
		} else {
			throw new Exception("Impossible d'acheter la carte : l'utilisateur n'a pas suffisament d'argent.");
		}

		return true;
	}
	
	public boolean sellCard(Integer userId, Integer cardId) throws Exception {
		CardModel cardModel = null; 
		UserModel userModel = null; 
		try {
			cardModel = getCardModel(cardId);
			userModel = getUserModel(userId);
		} catch (Exception e) {
			throw new Exception("Impossible de vendre la carte");
		}
		cardModel.setUserId(-1);
		userModel.setAccount(userModel.getAccount() + cardModel.getPrice());
		
		cardDAO.updateCard(cardModel, cardId);
		userDAO.updateUser(userModel, userId); 
		return true;
	}
	
	protected CardModel getCardModel(Integer cardId) throws Exception {
		ResponseEntity<CardModel> responseCardModel = cardDAO.getCard(cardId);
		CardModel CardModel;
		if (responseCardModel.getStatusCode().equals(HttpStatus.OK)) {
			CardModel = responseCardModel.getBody();
		} else {
			throw new Exception();
		}
		return CardModel;
	}

	protected UserModel getUserModel(Integer userId) throws Exception {
		ResponseEntity<UserModel> responseUserModel = userDAO.getUser(userId);
		
		UserModel userModel;
		if (responseUserModel.getStatusCode().equals(HttpStatus.OK)) {
			userModel = responseUserModel.getBody();
		} else {
			throw new Exception();
		}
		return userModel;
	}
	
}
