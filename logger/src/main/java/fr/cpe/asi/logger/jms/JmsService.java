package fr.cpe.asi.logger.jms;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.jms.Message;
import javax.jms.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.jms.Operation;
import fr.cpe.asi.data.model.jms.TypeOperation;

@Service
public class JmsService {

	public static final String JMS_QUEUE_CARD = "card";
	public static final String JMS_QUEUE_STORE = "store";
	public static final String JMS_QUEUE_USER = "user";
	public static final String JMS_QUEUE_CHAT = "chat";
	public static final String JMS_QUEUE_GAME = "game";
	public static final String FILE_PATH = "log.txt";
	private JmsTemplate jmsTemplate;
	private Queue queueCard;

    private Logger logger = LoggerFactory.getLogger(JmsService.class);

	public JmsService(JmsTemplate jmsTemplate, Queue queueCard) {
		this.jmsTemplate = jmsTemplate;
		this.queueCard = queueCard;
	}

	public static void writeLog(Message message, String queue) {
		System.out.println("Received a message"); 
		File file = new File(FILE_PATH);
		System.out.println(file.getAbsolutePath()); 
		try {
			if (!file.createNewFile()) {
				String content = queue + ":" + message; 
				FileOutputStream outputStream = new FileOutputStream(FILE_PATH);
			    byte[] strToBytes = content.getBytes();
			    outputStream.write(strToBytes);
			    outputStream.close();
			} 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
}
