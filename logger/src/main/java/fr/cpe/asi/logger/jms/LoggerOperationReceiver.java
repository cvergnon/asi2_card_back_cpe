package fr.cpe.asi.logger.jms;

import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import fr.cpe.asi.data.model.jms.Operation;


@Component
public class LoggerOperationReceiver {
	
    private Logger logger = LoggerFactory.getLogger(LoggerOperationReceiver.class);
	private LoggerOperationService cardOperationService;
	
	public LoggerOperationReceiver(LoggerOperationService cardOperationService) {
		this.cardOperationService = cardOperationService;
	}
	
	@JmsListener(destination = JmsService.JMS_QUEUE_CARD, containerFactory = "connectionFactory")
	public void receiveCardOperation(Message message) {
		logger.info("Réception du message {} sur {}", JmsService.JMS_QUEUE_CARD);
		JmsService.writeLog(message, JmsService.JMS_QUEUE_CARD); 
	}
	
	@JmsListener(destination = JmsService.JMS_QUEUE_USER, containerFactory = "connectionFactory")
	public void receiveUserOperation(Operation operation, Message message) {
		logger.info("Réception du message {} sur {}", operation, JmsService.JMS_QUEUE_USER);
		JmsService.writeLog(message, JmsService.JMS_QUEUE_USER); 
	}
	
	@JmsListener(destination = JmsService.JMS_QUEUE_CHAT, containerFactory = "connectionFactory")
	public void receiveChatOperation(Operation operation, Message message) {
		logger.info("Réception du message {} sur {}", operation, JmsService.JMS_QUEUE_CHAT);
		JmsService.writeLog(message, JmsService.JMS_QUEUE_CHAT); 
	}
	
	@JmsListener(destination = JmsService.JMS_QUEUE_GAME, containerFactory = "connectionFactory")
	public void receiveGameOperation(Operation operation, Message message) {
		logger.info("Réception du message {} sur {}", operation, JmsService.JMS_QUEUE_GAME);
		JmsService.writeLog(message, JmsService.JMS_QUEUE_GAME); 
	}
	
	@JmsListener(destination = JmsService.JMS_QUEUE_STORE, containerFactory = "connectionFactory")
	public void receiveStoreOperation(Operation operation, Message message) {
		logger.info("Réception du message {} sur {}", operation, JmsService.JMS_QUEUE_STORE);
		JmsService.writeLog(message, JmsService.JMS_QUEUE_STORE); 
	}

}

