package fr.cpe.asi.data.model.rest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;
import fr.cpe.asi.data.model.entities.game.GameModel;

@RequestMapping(value = GameRest.BASE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public interface GameRest {

	/*
	 * Definition des url de l'API pour le controller Card
	 */
	public static final String BASE_URL = "/api";
	
	public static final String ADD_GAME = "/game";
	
	/*
	 * Adresse et port de l'API Chat defini pour l'utilsation interne de l'application
	 */
	public static final String SERVER = "localhost:8085" + BASE_URL;

	
    ResponseEntity<HttpStatus> addGame(@RequestBody GameModel game);
    
    
    
}