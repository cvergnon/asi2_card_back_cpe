package fr.cpe.asi.data.model.entities.chat;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ChatModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	private int emitterId;
	private int receiverId;
	private Date timestamp;
	private String message;

	public ChatModel() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public int getEmitterId() {
		return emitterId;
	}

	public void setEmitterId(int emitterId) {
		this.emitterId = emitterId;
	}

	public int getReceiverid() {
		return receiverId;
	}

	public void setReceiverid(int receiverid) {
		this.receiverId = receiverid;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	
	
}
