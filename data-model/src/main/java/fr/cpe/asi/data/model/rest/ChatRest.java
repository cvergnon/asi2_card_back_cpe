package fr.cpe.asi.data.model.rest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.entities.chat.ChatModel;

@RequestMapping(value = ChatRest.BASE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public interface ChatRest {

	/*
	 * Definition des url de l'API pour le controller Card
	 */
	public static final String BASE_URL = "/api";
	
	public static final String GET_CONVERSATION = "/message/{emitterId}/{receiverId}";
	public static final String ADD_MESSAGE = "/message";
	
	/*
	 * Adresse et port de l'API Chat defini pour l'utilsation interne de l'application
	 */

	public static final String SERVER = "localhost:8082" + BASE_URL;
   
	/**
	 * Récupération d'une conversation entre deux utilisateurs
	 * @return List<ChatModel>
	 */
    @RequestMapping(
    		value = GET_CONVERSATION,
    		method = RequestMethod.GET
    )
    ResponseEntity<List<ChatModel>> getConversation(@PathVariable int emitterId, @PathVariable int receiverId);
    
	/**
	 * Ajout d'un message dans la base
	 */
    @RequestMapping(
    		value = ADD_MESSAGE,
    		method = RequestMethod.POST
    )
    ResponseEntity<HttpStatus> addMessage(@RequestBody ChatModel chat);
    
}