package fr.cpe.asi.data.model.jms;

public enum TypeOperation {
	PUT,
	DELETE,
	POST
}
