package fr.cpe.asi.data.model.rest;


import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import fr.cpe.asi.data.model.entities.card.CardModel;

public interface CardRest {

	/*
	 * Definition des url de l'API pour le controller Card
	 */
	public static final String BASE_URL = "/api";
	
	public static final String GET_CARDS = "/cards";
	public static final String GET_CARD_BY_ID = "/card/{id}";
	public static final String ADD_CARD = "/card";
	public static final String UPDATE_CARD = "/card/{id}";
	public static final String DELETE_CARD = "/card/{id}";
	public static final String GET_CARDS_TO_SELL = "/cards_to_sell";
	public static final String GET_CARDS_BY_USER = "/cards/{userId}";
	
	/*
	 * Adresse et port de l'API Card defini pour l'utilsation interne de l'application
	 */

	public static final String SERVER = "localhost:8084" + BASE_URL;
	
	/**
	 * Récupération de la liste de toutes les cartes
	 * @return List<CardLightModel> 
	 */
    ResponseEntity<List<CardModel>> getAllCards();
    
    
	/**
	 * Récupération d'une carte à partir de son identifiant
	 * @return CardLightModel
	*/
    ResponseEntity<CardModel> getCard(Integer id);
    
    
	/**
	 * Ajout d'une carte dans la base
	 */
    ResponseEntity<HttpStatus> addCard(CardModel card);
    
    
	/**
	 * Mise à jour d'une carte dans la base de données
	 * @return List<Card> 
	 */
    ResponseEntity<HttpStatus> updateCard(CardModel card, Integer id);
    
    
	/**
	 * Suppression d'une carte dans la base
	 */
    ResponseEntity<HttpStatus> deleteCard(Integer id);
    
    
	/**
	 * Récupération de la liste des cartes à vendre
	 */
    ResponseEntity<List<CardModel>> getCardsToSell();
    
    /**
     * Récupère les cartes d'un utilisateur
     */
    ResponseEntity<List<CardModel>> getCardsByUser(Integer userId);
}
