package fr.cpe.asi.data.model.api.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import fr.cpe.asi.data.model.entities.card.CardModel;
import fr.cpe.asi.data.model.rest.CardRest;

@Component
public class CardImpl implements CardRest {
	
	private RestTemplate restTemplate;

	
	public CardImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public ResponseEntity<List<CardModel>> getAllCards() {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(CardRest.SERVER).path(CardRest.GET_CARDS).build();
		ResponseEntity<CardModel[]> cards = restTemplate.getForEntity(uriComponents.toString(), CardModel[].class);
		return new ResponseEntity<List<CardModel>>(Arrays.asList(cards.getBody()), cards.getStatusCode());
	}

	public ResponseEntity<CardModel> getCard(Integer id) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(CardRest.SERVER).path(CardRest.GET_CARD_BY_ID).buildAndExpand(id);
		ResponseEntity<CardModel> card = restTemplate.getForEntity(uriComponents.toString(), CardModel.class);
		return new ResponseEntity<CardModel>(card.getBody(), card.getStatusCode());
	}

	public ResponseEntity<HttpStatus> addCard(CardModel card) {
	    UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(CardRest.SERVER).path(CardRest.ADD_CARD).build();
		return restTemplate.postForEntity(uriComponents.toString(), card, HttpStatus.class);
	}

	public ResponseEntity<HttpStatus> updateCard(CardModel card, Integer id) {
	    UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(CardRest.SERVER).path(CardRest.UPDATE_CARD).buildAndExpand(id);
		return restTemplate.exchange(uriComponents.toString(), HttpMethod.PUT, new HttpEntity<CardModel>(card), HttpStatus.class);
	}

	public ResponseEntity<HttpStatus> deleteCard(Integer id) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(CardRest.SERVER).path(CardRest.DELETE_CARD).buildAndExpand(id);
		return restTemplate.exchange(uriComponents.toString(), HttpMethod.DELETE, null, HttpStatus.class);
	}

	public ResponseEntity<List<CardModel>> getCardsToSell() {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(CardRest.SERVER).path(CardRest.GET_CARDS_TO_SELL).build();
		ResponseEntity<CardModel[]> cards = restTemplate.getForEntity(uriComponents.toString(), CardModel[].class);
		return new ResponseEntity<List<CardModel>>(Arrays.asList(cards.getBody()), cards.getStatusCode());
	}

	@Override
	public ResponseEntity<List<CardModel>> getCardsByUser(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}


}
