package fr.cpe.asi.data.model.jms;

import java.io.Serializable;

public class Operation implements Serializable {
	
	protected TypeOperation typeOperation;
		
	public Operation() {
		
	}
	
	public Operation(TypeOperation typeOperation) {
		this.typeOperation = typeOperation;
	}

	public TypeOperation getTypeOperation() {
		return typeOperation;
	}

	public void setTypeOperation(TypeOperation typeOperation) {
		this.typeOperation = typeOperation;
	}
}
