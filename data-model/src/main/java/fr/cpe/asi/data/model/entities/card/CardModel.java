package fr.cpe.asi.data.model.entities.card;


import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class CardModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String coupSpecial;
	private float hp;
	private float charisme;
	private float credibilite;
	private float price;
	private int userId;
	private String name;
	private String description;
	private String family;
	private String affinity;
	private String imgUrl;
	
	
	public CardModel() {
	}


	public CardModel(String name, String description, String family, String affinity, String coupSpecial, float hp,
					 float charisme, float credibilite,String imgUrl,float price) {
		this.name = name; 
		this.description = description; 
		this.family = family; 
		this.affinity = affinity; 
		this.coupSpecial = coupSpecial;
		this.hp = hp;
		this.charisme = charisme;
		this.credibilite = credibilite;
		this.price=price;
		this.imgUrl = imgUrl; 
	}
	
	public String getcoupSpecial() {
		return coupSpecial;
	}
	
	public void setcoupSpecial(String coupSpecial) {
		this.coupSpecial = coupSpecial;
	}
	
	public float getHp() {
		return hp;
	}
	
	public void setHp(float hp) {
		this.hp = hp;
	}
	
	public float getcharisme() {
		return charisme;
	}
	
	public void setcharisme(float charisme) {
		this.charisme = charisme;
	}
	
	public float getcredibilite() {
		return credibilite;
	}
	
	public void setcredibilite(float credibilite) {
		this.credibilite = credibilite;
	}

	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}

	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public float computePrice() {
		return this.hp * 20 + this.charisme*20 + this.credibilite*20;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getAffinity() {
		return affinity;
	}

	public void setAffinity(String affinity) {
		this.affinity = affinity;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

}
