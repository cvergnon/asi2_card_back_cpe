package fr.cpe.asi.data.model.api.impl;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import fr.cpe.asi.data.model.dto.StoreOrderDTO;
import fr.cpe.asi.data.model.rest.StoreRest;

@Component
public class StoreImpl implements StoreRest {

	private RestTemplate restTemplate;

	public StoreImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Override
	public ResponseEntity<Boolean> buyCards(StoreOrderDTO order) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(StoreRest.SERVER).path(StoreRest.BASE_URL + StoreRest.BUY_CARDS).build();
		return restTemplate.postForEntity(uriComponents.toString(), order, Boolean.class);
	}

	@Override
	public ResponseEntity<Boolean> sellCards(StoreOrderDTO order) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(StoreRest.SERVER).path(StoreRest.SELL_CARDS).build();
		return restTemplate.postForEntity(uriComponents.toString(), order, Boolean.class);
	}

}
