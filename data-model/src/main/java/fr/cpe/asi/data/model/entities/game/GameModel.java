package fr.cpe.asi.data.model.entities.game;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class GameModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id; 
	private int winnerId;
	private int loserId;
	private float bet; 
	
	public float getBet() {
		return bet;
	}
	public void setBet(float bet) {
		this.bet = bet;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	private float value;
	
	public int getWinnerId() {
		return winnerId;
	}
	public void setWinnerId(int winnerId) {
		this.winnerId = winnerId;
	}
	public int getLoserId() {
		return loserId;
	}
	public void setLoserId(int loserId) {
		this.loserId = loserId;
	}
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}


	
}
