package fr.cpe.asi.data.model.rest;


import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.cpe.asi.data.model.dto.StoreOrderDTO;



@RequestMapping(value = StoreRest.BASE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public interface StoreRest {

	/*
	 * Definition des url de l'API pour le controller Store
	 */
	public static final String BASE_URL = "/api";
	
	public static final String BUY_CARDS = "/buy";
	public static final String SELL_CARDS = "/sell";
	
	/*
	 * Adresse et port de l'API Card defini pour l'utilsation interne de l'application
	 */
	public static final String SERVER = "localhost:8083" + BASE_URL;
    
	/**
	 * Récupération de la liste de toutes les cartes
	 * @return List<CardLightModel> 
	 */
    ResponseEntity<Boolean> buyCards(StoreOrderDTO order);
    
    
	/**
	 * Récupération d'une carte à partir de son identifiant
	 * @return CardLightModel
	 */
    ResponseEntity<Boolean> sellCards(StoreOrderDTO order);
    
    
}