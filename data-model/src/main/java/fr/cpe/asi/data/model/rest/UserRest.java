package fr.cpe.asi.data.model.rest;


import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import fr.cpe.asi.data.model.entities.user.UserModel;


public interface UserRest {

	/*
	 * Definition des url de l'API pour le controller User
	 */
	
	public static final String BASE_URL = "/api";
	public static final String GET_USERS = "/users";
	public static final String GET_USER_BY_ID = "/user/{id}";
	public static final String ADD_USER = "/user";
	public static final String UPDATE_USER = "/user/{id}";
	public static final String DELETE_USER = "/user/{id}";
	public static final String AUTH = "/auth/{login}/{password}";
	
	/*
	 * Adresse et port de l'API User defini pour l'utilsation interne de l'application
	 */
	public static final String SERVER = "localhost:8086" + BASE_URL;

    
	/**
	 * Récupération de la liste de tous les utilisateurs
	 * @return List<UserModel> 
	 */

    ResponseEntity<List<UserModel>> getAllUsers();
    
   
	/**
	 * Récupération d'un utilisateur à partir de son identifiant
	 * @return UserModel
	 */
    ResponseEntity<UserModel> getUser(Integer id);
    
    
	/**
	 * Ajout d'un utilisateur dans la base
	 */
    ResponseEntity<UserModel> addUser(UserModel user);
    
    
	/**
	 * Mise à jour d'un utilisateur dans la base de données
	 */
    ResponseEntity<HttpStatus> updateUser(UserModel user, @PathVariable Integer id);
    
    
	/**
	 * Suppression d'un utilisateur de la base
	 */
    ResponseEntity<HttpStatus> deleteUser(Integer id);
    
    
	/**
	 * permet de gérer l'authentification
	 */
    ResponseEntity<UserModel> authenticate(String login, String password);
}