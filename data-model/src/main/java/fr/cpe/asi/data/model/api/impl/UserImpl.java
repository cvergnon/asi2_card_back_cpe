package fr.cpe.asi.data.model.api.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import fr.cpe.asi.data.model.entities.user.UserModel;
import fr.cpe.asi.data.model.rest.UserRest;

@Component
public class UserImpl implements UserRest {
	
	private RestTemplate restTemplate;
	
	public UserImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public ResponseEntity<List<UserModel>> getAllUsers() {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(UserRest.SERVER).path(UserRest.GET_USERS).build();
		ResponseEntity<UserModel[]> users = restTemplate.getForEntity(uriComponents.toString(), UserModel[].class);
		return new ResponseEntity<List<UserModel>>(Arrays.asList(users.getBody()), users.getStatusCode());
	}

	public ResponseEntity<UserModel> getUser(Integer id) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(UserRest.SERVER).path(UserRest.GET_USER_BY_ID).buildAndExpand(id);
		ResponseEntity<UserModel> user = restTemplate.getForEntity(uriComponents.toString(), UserModel.class);
		return new ResponseEntity<UserModel>(user.getBody(), user.getStatusCode());
	}

	public ResponseEntity<UserModel> addUser(UserModel user) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(UserRest.SERVER).path(UserRest.ADD_USER).build();
		return restTemplate.exchange(uriComponents.toString(), HttpMethod.POST, new HttpEntity<UserModel>(user), UserModel.class);
	}

	public ResponseEntity<HttpStatus> updateUser(UserModel user, Integer id) {
		 UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(UserRest.SERVER).path(UserRest.UPDATE_USER).buildAndExpand(id);
		return restTemplate.exchange(uriComponents.toString(), HttpMethod.PUT, new HttpEntity<UserModel>(user), HttpStatus.class);
	
	}

	public ResponseEntity<HttpStatus> deleteUser(Integer id) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(UserRest.SERVER).path(UserRest.DELETE_USER).buildAndExpand(id);
		return restTemplate.exchange(uriComponents.toString(), HttpMethod.DELETE, null, HttpStatus.class);
	
	}

	public ResponseEntity<UserModel> authenticate(String login, String password) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
	    	      .scheme("http").host(UserRest.SERVER).path(UserRest.AUTH + "/" + login + "/" + password).build();
		return restTemplate.exchange(uriComponents.toString(), HttpMethod.GET, null, UserModel.class);
	}

	

}
