package fr.cpe.asi.user.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.cpe.asi.data.model.entities.user.UserModel;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Integer> {
	
	List<UserModel> findByLoginAndPassword(String login,String password);

}
