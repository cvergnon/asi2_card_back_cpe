package fr.cpe.asi.user.jms;

import fr.cpe.asi.data.model.entities.user.UserModel;
import fr.cpe.asi.data.model.jms.Operation;
import fr.cpe.asi.data.model.jms.TypeOperation;

public class UserOperation extends Operation {

	private UserModel user;
	
	public UserOperation() {
		
	}
	
	public UserOperation(TypeOperation typeOperation, UserModel user) {
		this.user = user;
		this.typeOperation = typeOperation;
	}

	
	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}
	
	
}
