package fr.cpe.asi.user.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import fr.cpe.asi.data.model.entities.user.UserModel;
import fr.cpe.asi.user.jms.JmsService;




@Service
public class UserService {
	
	private JmsService jmsService;
	private UserRepository userRepository;
	
	public UserService(UserRepository userRepository, JmsService jmsService) {
		this.jmsService = jmsService;
		this.userRepository = userRepository;
	}
	
	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public Optional<UserModel> getUser(String id) {
		return userRepository.findById(Integer.valueOf(id));
	}

	public Optional<UserModel> getUser(Integer id) {
		return userRepository.findById(id);
	}

	public void addUser(UserModel user) {
		jmsService.createUser(user);
	}

	public void updateUser(UserModel user) {
		jmsService.updateUser(user);
	}

	public void deleteUser(Integer id) {
		UserModel user = new UserModel();
		user.setId(id);
		jmsService.deleteUser(user);

	}

	public UserModel getUserByLoginPwd(String login, String password) {
		List<UserModel> listUserModel = userRepository.findByLoginAndPassword(login, password);
		if (listUserModel.size() > 0) {
			return listUserModel.get(0);
		} else {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}
	}
	

	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
		UserModel userModel = new UserModel("axel", "axel"); 
		this.addUser(userModel);
		userModel = new UserModel("corentin", "corentin"); 
		this.addUser(userModel);
		userModel = new UserModel("vincent", "vincent"); 
		this.addUser(userModel);
		userModel = new UserModel("quentin", "quentin");
		userModel.setAccount(250000);
		this.addUser(userModel);
	}


}
