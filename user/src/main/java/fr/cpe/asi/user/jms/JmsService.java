package fr.cpe.asi.user.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import fr.cpe.asi.data.model.entities.user.UserModel;
import fr.cpe.asi.data.model.jms.TypeOperation;

@Service
public class JmsService {

    private Logger logger = LoggerFactory.getLogger(JmsService.class);
	public static final String JMS_QUEUE_USER = "/user";
	private JmsTemplate jmsTemplate;
	
	public JmsService(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}
	
	public void updateUser(UserModel user) {
		logger.info("Envoi du message {} sur {}", user, JMS_QUEUE_USER);
		jmsTemplate.convertAndSend(JMS_QUEUE_USER, new UserOperation(TypeOperation.PUT, user));
	}
	
	public void deleteUser(UserModel user) {
		logger.info("Envoi du message {} sur {}", user, JMS_QUEUE_USER);
		jmsTemplate.convertAndSend(JMS_QUEUE_USER, new UserOperation(TypeOperation.DELETE, user));
	}
	
	public void createUser(UserModel user) {
		logger.info("Envoi du message {} sur {}", user, JMS_QUEUE_USER);
		jmsTemplate.convertAndSend(JMS_QUEUE_USER, new UserOperation(TypeOperation.POST, user));
	}
	
}
