package fr.cpe.asi.user.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.cpe.asi.data.model.entities.user.UserModel;
import fr.cpe.asi.data.model.rest.UserRest;


@CrossOrigin
@RestController
@RequestMapping(value = UserRest.BASE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRestController implements UserRest {
	
	private UserService userService;
    private Logger logger = LoggerFactory.getLogger(UserRestController.class);
	
	public UserRestController(UserService userService) {
		this.userService = userService;
	}
	

	@Override
    @RequestMapping(
    		value = GET_USERS,
    		method = RequestMethod.GET
    )
	public ResponseEntity<List<UserModel>> getAllUsers() {
		logger.trace("Call getAllUsers()");
		return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
	}

	@Override
    @RequestMapping(
    		value = GET_USER_BY_ID,
    		method = RequestMethod.GET
    )
	public ResponseEntity<UserModel> getUser(@PathVariable Integer id) {
		logger.trace("Call getUser({})", id);
		Optional<UserModel> ruser;
		ruser= userService.getUser(id);
		if(ruser.isPresent()) {
			return new ResponseEntity<>(ruser.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Override
    @RequestMapping(
    		value = ADD_USER,
    		method = RequestMethod.POST
    )
	public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
		logger.trace("Call addUser({})", user);
		userService.addUser(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@Override
    @RequestMapping(
    		value = UPDATE_USER,
    		method = RequestMethod.PUT
    )
	public ResponseEntity<HttpStatus> updateUser(@RequestBody UserModel user, @PathVariable Integer id) {		
		logger.trace("Call updateUser({})", user);
		user.setId(id);
		userService.updateUser(user);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
    @RequestMapping(
    		value = DELETE_USER,
    		method = RequestMethod.DELETE
    )
	public ResponseEntity<HttpStatus> deleteUser(@PathVariable Integer id) {
		logger.trace("Call deleteUser({})", id);
		userService.deleteUser(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
    @RequestMapping(
    		value = AUTH,
    		method = RequestMethod.GET
    )
	public ResponseEntity<UserModel> authenticate(@PathVariable String login, @PathVariable String password) {
		return new ResponseEntity<>(userService.getUserByLoginPwd(login, password), HttpStatus.OK);
	}


}
