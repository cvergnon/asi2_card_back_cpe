package fr.cpe.asi.user.jms;

import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class UserOperationReceiver {
	
    private Logger logger = LoggerFactory.getLogger(UserOperationReceiver.class);
	private UserOperationService cardOperationService;
	
	public UserOperationReceiver(UserOperationService cardOperationService) {
		this.cardOperationService = cardOperationService;
	}

	@JmsListener(destination = JmsService.JMS_QUEUE_USER, containerFactory = "connectionFactory")
	public void receiveUserOperation(UserOperation cardOperation, Message message) {
		logger.info("Réception du message {} sur {}", cardOperation, JmsService.JMS_QUEUE_USER);

		switch (cardOperation.getTypeOperation()) {
			case DELETE:
				cardOperationService.deleteUser(cardOperation.getUser().getId());
				break;
			case POST:
				cardOperationService.createUser(cardOperation.getUser());
				break;
			case PUT:
				cardOperationService.updateUser(cardOperation.getUser());
				break;
			default:
				break;
		}
	}

}

