package fr.cpe.asi.user.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.cpe.asi.data.model.entities.user.UserModel;
import fr.cpe.asi.user.service.UserRepository;

@Service
public class UserOperationService {

	private UserRepository userRepository;
	

    private Logger logger = LoggerFactory.getLogger(UserOperationService.class);
	
	public UserOperationService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void deleteUser(Integer id) {
		logger.info("Delete user {}", id);
		userRepository.deleteById(id);
	}

	public void createUser(UserModel user) {
		logger.info("Create user {}", user.getId());
		userRepository.save(user);
	}

	public void updateUser(UserModel user) {
		logger.info("Update user {}", user.getId());
		userRepository.save(user);
	}
	
}
